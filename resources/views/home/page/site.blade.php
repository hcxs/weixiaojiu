@extends('home.page.public')
@section('title', "微小酒")
<link rel="stylesheet" type="text/css" href="/css/site.css">
<link rel="stylesheet" type="text/css" href="/css/search.css"/>
<link rel="stylesheet" type="text/css" href="/37banner/css/normalize.css"/>
@section('link')
@endsection
@section('css')
    <style type="text/css">
        a {
            color: #ffffff;
            display: block;
        }
        img {
            width: 100%;
        }
        .items-container {
            width: 100%;
            /*background: #ffffff;*/
        }
        .items-container li {
            width: 50%;
            float: left;
            margin-top: 5px;
        }
        .item-content {
            background: #ffffff;
        }
        .item-txt {
            padding: 15px 10px;
            font-size: 15px;
            line-height: 1.2em;
            color: #343237;
        }
        .item-txt p:nth-child(1) {
            letter-spacing: -1px;
        }
        .item-txt p:nth-child(2) {
            font-size: .8em;
            color: #6F599C;
        }
        .item-txt p:nth-child(3) {
            padding-top: .5em;
            color: orange;
        }
        .item-txt p span {
            font-size: 1.4em;
        }
        .items-container li:nth-child(2n+1) div:first-child {
             margin-right: 2.5px;
         }
        .items-container li:nth-child(2n+0) div:first-child{
            margin-left: 2.5px;
        }
    </style>
@endsection
@section('content')
    {{--<form id="search-bar">--}}
        {{--<div>--}}
            {{--<input type="text" placeholder="请输入要搜索的关键字..">--}}
            {{--<button type="button"><img style="width: 2em;" src="/images/icon-search.png"></button>--}}
        {{--</div>--}}
    {{--</form>--}}
    <div id="top-bar">
        <form method="" action="" class="top-bar-content">
            <div class="top-bar-content-left top-bar-back">
                {{--<a href="javascript:history.go(-1)">返回</a>--}}
                <a href="javascript:history.go(-1)"><img style="position: absolute;top:15px;width: 20px;left: 10px;" src="/images/icon-back3.png"></a>
            </div>
            <div class="top-bar-content-right">
                <div class="top-bar-content-right1">
                    <div style="flex: 2 0 0;position: relative">
                        <input style="position: absolute;top: 12px;" type="text" name="txt" placeholder="输入商品关键字"/>
                    </div>
                    <div>
                        <button type="submit">搜索</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="div1">
        <img style="width: 100%" src="/images/wxj.png">
    </div>
    {{--<div class="div2">今日特卖</div>--}}
    <div id="show">
        {{--@foreach($items as $item)--}}
            {{--<a class="div3" href="/at/m/goods/{{ $item->goods->id }}">--}}
                {{--<div class="div3-item">--}}
                    {{--<img src="{{ $item->goods->preview }}">--}}
                {{--</div>--}}
                {{--<div class="div3-item">--}}
                    {{--<p>{{ $item->goods->name }}</p>--}}
                    {{--<p>{{ $item->summary }}</p>--}}
                    {{--<p>--}}
                        {{--<span>￥<span style="font-size: 1.4em;">{{ number_format($item->goods->price * $item->cut/10, 2, '.', '') }}</span></span>--}}
                        {{--@if($item->goods->cut == 10)--}}

                        {{--@else--}}
                            {{--<span style="text-decoration: line-through;color: gray;">￥{{ number_format($item->goods->price, 2, '.', '') }}</span>--}}
                        {{--@endif--}}
                    {{--</p>--}}
                    {{--<img class="add" src="/images/icon-cart.png">--}}
                {{--</div>--}}
            {{--</a>--}}
        {{--@endforeach--}}
        <ul class="items-container">
            @foreach($items as $item)
               <li>
                   <a href="/at/m/goods/{{ $item->goods->id }}">
                       <div class="item-content">
                           <img src="{{ $item->goods->preview }}">
                           <div class="item-txt">
                               <p>{{ mb_strimwidth($item->goods->name, 0, 40, "...") }}</p>
                               <p>{{ $item->summary }}</p>
                               <p>￥<span>{{ number_format($item->goods->price, 2, '.', '') }}</span></p>
                           </div>
                       </div>
                   </a>
               </li>
            @endforeach
        </ul>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var winHeight = $(document).scrollTop();
            $(window).scroll(function () {
                var scrollY = $(document).scrollTop();// 获取垂直滚动的距离，即滚动了多少
                if (scrollY > 60) { //如果滚动距离大于550px则隐藏，否则删除隐藏类
                    $('#top-bar').addClass('hidden');
                }
                else {
                    $('#top-bar').removeClass('hidden');
                }

                if (scrollY > winHeight) { //如果没滚动到顶部，删除显示类，否则添加显示类
                    $('#top-bar').removeClass('showed');
                }
                else {
                    $('#top-bar').addClass('showed');
                }

            });
        });
//        function add(id) {
//            $.ajax({
//                url: "/service/cart/add",
//                type: "get",
//                data: {id: id, num: 1},
//                timeout: 3000,
//                dataType: "json",
//                success: function (data) {
//                    console.log(data.status);
//                    if (data.status != 0) {
//                        $(".toptips").show();
//                        $(".toptips span").html("服务器出小差了，请稍后再试");
//                        setTimeout(function () {
//                            $(".toptips").hide();
//                        }, 2000);
//                        return false;
//                    }
//                    if (data.status == 0) {
//                        $(".toptips").show();
//                        $(".toptips span").html(data.message);
//                        setTimeout(function () {
//                            $(".toptips").hide();
//                        }, 2000);
////                        $("#cart").attr("src", "/images/icon-cart-smile.png");
//                    }
//                },
//                error: function (xhr, status, error) {
////                    console.log(xhr);
////                    console.log(status);
////                    console.log(error);
//                }
//            });
//        }
    </script>
@endsection