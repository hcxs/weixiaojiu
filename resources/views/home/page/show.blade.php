@extends('home.page.public')
@section('title', "$goods->name")
@section('link')
    <link rel="stylesheet" type="text/css" href="/37banner/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/fxsmall.css"/>
    <script src="/37banner/js/modernizr.custom.js"></script>
@endsection
@section('css')
    <style type="text/css">
        img {
            width: 100%;
        }

        nav {
            display: block !important;
        }
        .weui_cells_access .weui_cell_ft:after {
            height: 10px;
            width: 10px;
        }

        .back {
            width: 2em;
            height: 2em;
            display: inline-block;
            position: absolute;
            top: 10px;
            left: 10px;
            z-index: 9999;
        }
        section {
            padding: 0;
        }
        .component {
            margin-bottom: 0;
        }
        .component-small {
            width: 100%;
            height: 200px;
        }
        .component-small > ul {
            width: 100%;
            z-index: 0;
        }
        .component li img {
            margin: 0 auto;
        }
        .component-small nav a.prev {
            top: 30%;
            left: 0;
            margin-left: 0;
        }
        .component-small nav a.next {
            top: 30%;
            right: 0;
            margin-left: 0;
        }
        .weui_cell {
            padding: 10px;
            background: #ffffff;
        }

        .weui_cell:before {
            left: 0;
        }

        .clear-flex {
            flex: none;
            -webkit-flex: none;
            color: gray;
            font-size: .9em;
        }

        .detail-container {
            padding-bottom: 60px;
            padding-top: 10px;
        }

        .detail-container img {
            padding-bottom: 10px;
            width: 100%;
        }

        .attribute-container {
            height: auto;
            position: relative;
        }

        .titel {
            /*border-bottom: 1px solid #eee;*/
            background: #ffffff;
            font-weight: bold;
            padding: 10px 60px 0 10px;
        }

        .attribute-container img {
            width: 30px;
            /*padding-right: 5px;*/
            vertical-align: bottom;
        }

        .buy-container {
            position: fixed;
            bottom: 0;
            background: #ffffff;
            width: 100%;
            height: 51px;
        }

        .c2 {
            color: #8a8a8a;
            background: #ffffff;
            padding-left: 10px;
            line-height: 1.4em;
            /*letter-spacing: -1px;*/
        }

        .c2 img {
            width: 20px;
            vertical-align: top;
        }

        .placeholder {
            border: none;
            background: none;
            color: inherit;
            text-align: left;
            line-height: 1.6em;
            height: 1.7em;
            font-size: 0.8em;
            margin: 10px 0 0 0;
        }

        .share {
            position: absolute;
            top: 10px;
            right: 5px;
            font-size: 12px;
            text-align: center;
        }

        .share img {
            width: 32px;
        }

        .c3 {
            height: 1em;
            line-height: 1em;
            padding-right: 2em;
            color: gray;
            font-size: .8em;
        }

        .c3 img {
            width: 1.6em;
            padding-right: .4em;
            vertical-align: text-bottom;
        }

        .price {
            color: orange;
            font-size: 1.4em;
            font-weight: bold;
        }

        .cut {
            color: #ffffff;
            padding: 0 5px;
            background: #6F599C;
        }

        .buy-1 {
            padding: 5px 5px 0 5px;
            display: inline-block;
            text-align: center;
            margin-left: 10px;
        }

        .buy-1 img {
            vertical-align: bottom;
            width: 1.6em;
        }

        .buy-1 p {
            font-size: .6em;
        }

        .buy-container .buy-2 {
            display: inline-block;
            float: right;
            height: inherit;
            line-height: 51px;
            padding: 0 12px;
            color: #ffffff;
            background: #6F599C;
        }
        .buy-container {
            max-width:720px;
        }
        .buy-2-1 {
            background: orange !important;
        }

        .number-plus, .number-sub {
            width: 24px;
            display: inline-block;
            text-align: center;
            line-height: 24px;
            /*background: #6f599c;*/
            /*color: #ffffff !important;*/
            color: gray !important;
            /*border-radius: 50%;*/
            vertical-align: top;
        }
        .count-container {
            display: inline-block;
        }
        .count-container input {
            width: 50px;
            text-align: center;
            height: 24px;
            font-size: 1.2em;
            border-radius: 4px;
            outline: none;
            /*border: none;*/
        }
    </style>
@endsection
@section('content')
    @extends('home.component.toast')
    <section>
        <div class="custom-select" hidden>
            <select id="fxselect" name="fxselect">
                <option value="-1" selected>选择一种效果...</option>
                <option value="fxCorner">Corner scale</option>
                <option value="fxVScale">Vertical scale</option>
                <option value="fxFall">Fall</option>
                <option value="fxFPulse">Forward pulse</option>
                <option value="fxRPulse">Rotate pulse</option>
                <option value="fxHearbeat">Hearbeat</option>
                <option value="fxCoverflow">Coverflow</option>
                <option value="fxRotateSoftly">Rotate me softly</option>
                <option value="fxDeal">Deal 'em</option>
                <option value="fxFerris">Ferris wheel</option>
                <option value="fxShinkansen">Shinkansen</option>
                <option value="fxSnake">Snake</option>
                <option value="fxShuffle">Shuffle</option>
                <option value="fxPhotoBrowse">Photo Browse</option>
                <option selected value="fxSlideBehind">Slide Behind</option>
                <option value="fxVacuum">Vacuum</option>
                <option value="fxHurl">Hurl it</option>
            </select>
        </div>
        <div id="component" class="component component-small">
            <ul class="itemwrap">
                {{--<li class="current"><img src="/images/banner1.jpg" alt="img01"/></li>--}}
                <li class="current"><img src="/37banner/images/01.jpg" alt="img02"/></li>
                <li><img src="/37banner/images/02.jpg" alt="img03"/></li>
                <li><img src="/37banner/images/03.jpg" alt="img04"/></li>
                <li><img src="/37banner/images/01.jpg" alt="img05"/></li>
            </ul>
            <nav>
                <a class="prev" href="#"></a>
                <a class="next" href="#"></a>
            </nav>
        </div>
    </section>
    <div class="attribute-container">
        <a class="share">
            <img src="/images/fenxiangyoujiang.png"><br>
            分享有奖
        </a>

        <div class="titel">
            @if ($goods->attr->attribute == 0)
                <img src="/images/icon-remai.png">
            @elseif ($goods->attr->attribute == 2)
                <img src="/images/icon-sale.png">
            @else
                <img src="/images/icon-cut.png">
            @endif
            {{ $goods->name }}
        </div>
        <div class="c2">
            <div>
                <span class="price">￥{{ number_format($goods->price*$goods->attr->cut*0.1, 2, '.', '') }}</span>
                @if ($goods->attr->cut == 10)
                    {{--<span class="price">￥{{ $goods->price }}</span>--}}
                @else
                    <span style="text-decoration: line-through;">￥{{ $goods->price }}</span>
                    <span class="cut">{{ $goods->attr->cut }}折</span>
                @endif
            </div>
        </div>
        <div class="c2">
            <div class="weui-flex">
                <div class="weui-flex-item">
                    <div class="placeholder">快递：免运费</div>
                </div>
                <div class="weui-flex-item">
                    <div class="placeholder">销量{{ $goods->sale->sales }}笔</div>
                </div>
                <div class="weui-flex-item">
                    <div class="placeholder">
                        <img style="width: 1.6em;padding-right: 2px;" src="/images/zhengping.png">质量保证
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="weui_cell">
        <div>
            <span>件数：</span>
            <div class="count-container">
                {{--<a class="number-sub">-</a>--}}
                <img src="/images/icon-sub.png" class="number-sub">
                <input id="num" pattern="[0-9]*" class="number-input" value='1' data-min="0"
                       data-max="10"
                       data-step="1" readonly>
                <img src="/images/icon-plus.png" class="number-plus">
                {{--<a class="number-plus">+</a>--}}
            </div>
        </div>
    </div>
    <div class="weui_panel">
        <div class="weui_panel_bd">
            <div class="weui_media_box weui_media_small_appmsg">
                <div class="weui_cells weui_cells_access">
                    <a class="weui_cell" href="javascript:;">
                        <div class="weui_cell_hd c3">
                            <img src="/images/gou.png">摔坏包赔
                        </div>
                        <div class="weui_cell_bd weui_cell_primary c3">
                            <img src="/images/gou.png">48小时内发货
                        </div>
                        <span class="weui_cell_ft"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="weui_panel" style="margin-top: 0;">
        <div class="weui_panel_bd">
            <div class="weui_media_box weui_media_small_appmsg">
                <div class="weui_cells weui_cells_access">
                    <a class="weui_cell" href="javascript:;">
                        <div class="weui_cell_hd c3" style="width: 100%;">
                            <img src="/images/pinglun.png">查看评论
                        </div>
                        <span class="weui_cell_ft"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="detail-container">
        <img src="/images/chanpingxiangqing.png">

        <p><img src="/images/detail.jpg"></p>
    </div>
    <div class="buy-container">
        {{--<div class="weui_btn_area weui_btn_area_inline">--}}
        {{--<a href="javascript:;" class="weui_btn weui_btn_primary" onclick="addCart({{ $goods->id }})">加入购物车</a>--}}
        {{--<a href="javascript:;" class="weui_btn weui_btn_primary">结算</a>--}}
        {{--</div>--}}
        <a class="buy-1" href="/at/m"><img src="/images/icon-home-circle.png">

            <p>进 店</p></a>
        <a class="buy-1" href="/at/m/cart"><img id="cart" src="/images/icon-cart.png">

            <p>购物车</p></a>
        <a class="buy-2 wxj-btn" href="javascript:;">立即购买</a>
        <a class="buy-2 buy-2-1 wxj-btn" href="javascript:;" onclick="addCart({{ $goods->id }})">加入购物车</a>
    </div>
    <!-- /container -->
    <script src="/37banner/js/classie.js"></script>
    <script src="/37banner/js/main.js"></script>
    <script>
        var total = window.localStorage;
        total['num'];
        function addCart(id) {
            $.ajax({
                url: "/service/cart/add",
                type: "get",
                data: {id: id, num: $("#num").val()},
                timeout: 3000,
                dataType: "json",
                success: function (data) {
                    console.log(data.status);
                    if (data.status != 0) {
                        $(".toptips").show();
                        $(".toptips span").html("服务器出小差了，请稍后再试");
                        setTimeout(function () {
                            $(".toptips").hide();
                        }, 2000);
                        return false;
                    }
                    if (data.status == 0) {
                        $(".toptips").show();
                        $(".toptips span").html(data.message);
                        setTimeout(function () {
                            $(".toptips").hide();
                        }, 2000);
                        if (!total['num']) {
                            total['num'] = 0 + parseInt(data.data);
                            return;
                        }
                        total['num'] = parseInt(total['num']) + parseInt(data.data);
//                        $("#cart").attr("src", "/images/icon-cart-smile.png");
                    }
                },
                error: function (xhr, status, error) {
//                    console.log(xhr);
//                    console.log(status);
//                    console.log(error);
                }
            });
        }
        function upDownOperation(element) {
            var _input = element.parent().find('input'),
                    _value = _input.val(),
                    _step = _input.attr('data-step') || 1;
            //检测当前操作的元素是否有disabled，有则去除
            element.hasClass('disabled') && element.removeClass('disabled');
            //检测当前操作的元素是否是操作的添加按钮（.input-num-up）‘是’ 则为加操作，‘否’ 则为减操作
            if (element.hasClass('number-plus')) {
                var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
                        _max = _input.attr('data-max') || false,
                        _down = element.parent().find('.number-sub');

                //若执行‘加’操作且‘减’按钮存在class='disabled'的话，则移除‘减’操作按钮的class 'disabled'
                _down.hasClass('disabled') && _down.removeClass('disabled');
                if (_max && _new_value >= _max) {
                    _new_value = _max;
                    element.addClass('disabled');
                }
            } else {
                var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
                        _min = _input.attr('data-min') || false,
                        _up = element.parent().find('.number-plus');
                //若执行‘减’操作且‘加’按钮存在class='disabled'的话，则移除‘加’操作按钮的class 'disabled'
                _up.hasClass('disabled') && _up.removeClass('disabled');
                if (_min && _new_value <= _min) {
                    _new_value = _min;
                    element.addClass('disabled');
                }
            }
            _input.val(_new_value);
        }

        $(function () {
            $('.number-plus').click(function () {
                upDownOperation($(this));
            });
            $('.number-sub').click(function () {
                upDownOperation($(this));
            });
        });
    </script>
@endsection