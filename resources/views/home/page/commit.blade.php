@extends('home.page.public')
@section('title', "订单确认")
@section('link')

    @endsection
@section('css')
    <style type="text/css">
        .items-container {
            width: 100%;
            display: flex;
            display: -webkit-flex;
        }
    </style>
    @endsection
@section('content')
    <div>
        <a href="javascript:history.go(-1)">回去</a>
    </div>
    <div class="items-container">
        <ul>

        </ul>
        <div>商品名称</div>
        <div>价格</div>
        <div>数量</div>
        <div>总价</div>
        <div>赠送积分</div>
    </div>
    @endsection