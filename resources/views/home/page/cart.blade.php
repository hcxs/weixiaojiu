@extends('home.page.public')
@section('title', "购物车")
@section('link')
    <link rel="stylesheet" type="text/css" href="/css/cart.css"/>
    @endsection
@section('css')
    <style type="text/css">
        body {
            background: #f9f9f9;
        }
        .bg-orange {
            min-width: 150px;
            margin: 5px 10px;
            float: right;
            background: #6F599C;
        }
    </style>
    @endsection
@section('content')
    <!--BEGIN dialog1-->
    <div class="js_dialog" id="iosDialog1" style="display: none;">
        <div class="weui-mask"></div>
        <div class="weui-dialog">
            <div class="weui-dialog__hd">
                {{--<strong class="weui-dialog__title">提示</strong>--}}
                <img style="width: 6em" src="/images/guidi.jpg">
            </div>
            <div class="weui-dialog__bd">您确定要删除吗?</div>
            <div class="weui-dialog__ft">
                <div class="weui-dialog__btn weui-dialog__btn_default" onclick="removeItem()">是的</div>
                <div class="weui-dialog__btn weui-dialog__btn_primary" onclick="hideDialog()">我再想想</div>
            </div>
        </div>
    </div>
    <!--END dialog1-->
    <div class="title-bar">
        <label class="back">
            <a href="javascript:history.go(-1)">
                <img src="/images/icon-back2.png">返回
            </a>
        </label>
        {{--<a class="back" href="javascript:history.go(-1)"><span>返回</span></a>--}}
        购物车
    </div>
    <div id="content">
        @foreach($items as $item)
            <div class="item-container">
                <div class="item-content">
                    <img src="{{ $item['attributes']['preview'] }}">
                </div>
                <div class="item-content">
                    <div>
                        <p>{{ mb_strimwidth($item['name'], 0, 40, '...') }}</p>
                        <p>{{ $item['attributes']['summary'] }}</p>
                        <p>￥<span>{{ number_format($item['price'], 2, '.', '') }}</span></p>
                        <div class="count-container">
                            {{--<p></p>--}}
                            数量：
                            <img src="/images/icon-sub.png" class="number-sub" onclick="sub({{ $item['id'] }})">
                            <input id="num" pattern="[0-9]*" class="number-input" value='{{ $item['quantity'] }}' data-min="1"
                                   data-max="10"
                                   data-step="1" readonly>
                            <img src="/images/icon-plus.png" class="number-plus" onclick="plus({{ $item['id'] }})">
                        </div>
                    </div>
                    <div class="item-content-right">
                        <img style="width: 1.2em" src="/images/icon-cross.png" onclick="showDialog({{ $item['id'] }})">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="bottom">
        <span style="margin-left: 10px;">合计：￥<span id="total_num">{{ $total }}</span></span>
        <a href="/at/m/commit" class="weui_btn bg-orange weui_btn_inline">前往结算</a>
    </div>
<script type="text/javascript">
    var sid;
    var total = window.localStorage;
    $("#total_num").html(total.num);
    function hideDialog() {
        $("#iosDialog1").css("display", "none");
    }

    function showDialog(id) {
        sid = id;
        $("#iosDialog1").css("display", "block");
    }
    function removeItem(id) {
        $.ajax({
            url: "/service/cart/remove",
            type: "get",
            data: {id: sid},
            timeout: 3000,
            dataType: "json",
            success: function (data) {
                console.log(data.status);
                if (data.status != 0) {
                    $(".toptips").show();
                    $(".toptips span").html("服务器出小差了，请稍后再试");
                    setTimeout(function () {
                        $(".toptips").hide();
                    }, 2000);
                    return false;
                }
                if (data.status == 0) {
                    total.num = parseInt(total.num) - parseInt(data.data);
                    location.reload();
                }
            },
            error: function (xhr, status, error) {
//                    console.log(xhr);
//                    console.log(status);
//                    console.log(error);
            }
        });
    }

    function plus(id) {
        $.ajax({
            url: "/service/cart/add",
            type: "get",
            data: {id: id, num: 1},
            timeout: 3000,
            dataType: "json",
            beforeSend: function () {
                $("#total_num").html("计算中...");
            },
            success: function (data) {
                if (data.status != 0) {
                    $(".toptips").show();
                    $(".toptips span").html("服务器出小差了，请稍后再试");
                    setTimeout(function () {
                        $(".toptips").hide();
                    }, 2000);
                    return false;
                }
                if (data.status == 0) {
                    total.num = parseInt(total.num) + parseInt(data.data);
                    $("#total_num").html(total.num);
                }
            },
            error: function (xhr, status, error) {
//                    console.log(xhr);
//                    console.log(status);
//                    console.log(error);
            }
        });
    }

    function sub(id) {

        $.ajax({
            url: "/service/cart/inc",
            type: "get",
            data: {id: id, num: 1},
            timeout: 3000,
            dataType: "json",
            beforeSend: function () {
                $("#total_num").html("计算中...");
            },
            success: function (data) {
                if (data.status != 0) {
                    $(".toptips").show();
                    $(".toptips span").html("服务器出小差了，请稍后再试");
                    setTimeout(function () {
                        $(".toptips").hide();
                    }, 2000);
                    return false;
                }
                if (data.status == 0) {
                    if (parseInt(total.num) - parseInt(data.data) == 0) {
                        $("#total_num").html(total.num);
                        return false;
                    }
                    total.num = parseInt(total.num) - parseInt(data.data);
                    $("#total_num").html(total.num);
                }
            },
            error: function (xhr, status, error) {
//                    console.log(xhr);
//                    console.log(status);
//                    console.log(error);
            }
        });
    }

    function returnFloat(value) {
        var value = Math.round(parseFloat(value) * 100) / 100;
        var xsd = value.toString().split(".");
        if (xsd.length == 1) {
            value = value.toString() + ".00";
            return value;
        }
        if (xsd.length > 1) {
            if (xsd[1].length < 2) {
                value = value.toString() + "0";
            }
            return value;
        }
    }

    function upDownOperation(element) {
        var _input = element.parent().find('input'),
            _value = _input.val(),
            _step = _input.attr('data-step') || 1;
        //检测当前操作的元素是否有disabled，有则去除
        element.hasClass('disabled') && element.removeClass('disabled');
        //检测当前操作的元素是否是操作的添加按钮（.input-num-up）‘是’ 则为加操作，‘否’ 则为减操作
        if (element.hasClass('number-plus')) {
            var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
                _max = _input.attr('data-max') || false,
                _down = element.parent().find('.number-sub');

            //若执行‘加’操作且‘减’按钮存在class='disabled'的话，则移除‘减’操作按钮的class 'disabled'
            _down.hasClass('disabled') && _down.removeClass('disabled');
            if (_max && _new_value >= _max) {
                _new_value = _max;
                element.addClass('disabled');
            }
        } else {
            var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
                _min = _input.attr('data-min') || false,
                _up = element.parent().find('.number-plus');
            //若执行‘减’操作且‘加’按钮存在class='disabled'的话，则移除‘加’操作按钮的class 'disabled'
            _up.hasClass('disabled') && _up.removeClass('disabled');
            if (_min && _new_value <= _min) {
                _new_value = _min;
                element.addClass('disabled');
            }
        }
        _input.val(_new_value);
    }

    $(function () {
        $('.number-plus').click(function () {
            upDownOperation($(this));
        });
        $('.number-sub').click(function () {
            upDownOperation($(this));
        });
    });
</script>
    @endsection