<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/common.css"/>
    <link rel="stylesheet" href="/css/weui.css"/>
    <link rel="stylesheet" href="/css/weui2.css"/>
    {{--<link rel="stylesheet" href="/css/weui3.css"/>--}}
    <script src="/js/jquery-1.11.2.min.js"></script>
    @yield('link')
</head>
@yield('css')
<style type="text/css">
    .toptips {
        display: none;
        position: fixed;
        bottom: 80px;
        text-align: center;
        width: 100%;
        z-index: 999;
    }

    .toptips span {
        background: #444444;
        color: #ffffff;
        padding: 15px 15px;
        border-radius: 5px;
        opacity: 0.9;
    }
</style>
<body>
{{--<提示组件--}}
<div class="toptips"><span></span></div>
{{--加载组件--}}
{{--@extends('compoent.loading')--}}
<!--content-->
@yield('content')
@yield('js')
</body>
</html>
