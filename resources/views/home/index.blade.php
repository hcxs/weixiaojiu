@extends('home.public')
@section('title', "微小酒")
@section('link')
    <link rel="stylesheet" type="text/css" href="/css/index.css"/>
    <link rel="stylesheet" type="text/css" href="/css/search.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="/37banner/css/fxsmall.css"/>
    <script src="/37banner/js/modernizr.custom.js"></script>
@endsection
@section('css')
    <style type="text/css">
        body {
            margin: 50px 0 !important;
        }

        section {
            padding: 0;
        }

        .component {
            margin-bottom: 0;
        }

        .component-small {
            width: 100%;
            height: 200px;
        }

        .component-small > ul {
            width: 100%;
            z-index: 0;
        }

        .component li img {
            margin: 0 auto;
        }

        .component-small nav a.prev {
            top: 30%;
            left: 0;
            margin-left: 0;
        }

        .component-small nav a.next {
            top: 30%;
            right: 0;
            margin-left: 0;
        }

        .bg-orange-b {
            color: #6F599C;
            border-color: #6F599C;
        }

        .weui_btn {
            min-width: 100px;
            min-height: 35px;
            width: 20%;
            padding-right: 0;
            padding-left: 0;
            font-size: .9em;
            border-radius: 17.5px;
        }
    </style>
@endsection
@section('content')
    {{--搜索--}}
    <div id="top-bar">
        <form method="" action="" class="top-bar-content">
            <div class="top-bar-content-left">
                <img src="/images/logo.png">
            </div>
            <div class="top-bar-content-right">
                <div class="top-bar-content-right1">
                    <div style="flex: 2 0 0;">
                        <input type="text" name="txt" placeholder="输入商品关键字"/>
                    </div>
                    <div>
                        <button type="submit">搜索</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--结束--}}
    <div class="container">
        <section>
            <div class="custom-select" hidden>
                <select id="fxselect" name="fxselect">
                    <option value="-1" selected>选择一种效果...</option>
                    <option value="fxCorner">Corner scale</option>
                    <option value="fxVScale">Vertical scale</option>
                    <option value="fxFall">Fall</option>
                    <option value="fxFPulse">Forward pulse</option>
                    <option value="fxRPulse">Rotate pulse</option>
                    <option value="fxHearbeat">Hearbeat</option>
                    <option value="fxCoverflow">Coverflow</option>
                    <option value="fxRotateSoftly">Rotate me softly</option>
                    <option value="fxDeal">Deal 'em</option>
                    <option value="fxFerris">Ferris wheel</option>
                    <option value="fxShinkansen">Shinkansen</option>
                    <option selected value="fxSnake">Snake</option>
                    <option value="fxShuffle">Shuffle</option>
                    <option value="fxPhotoBrowse">Photo Browse</option>
                    <option value="fxSlideBehind">Slide Behind</option>
                    <option value="fxVacuum">Vacuum</option>
                    <option value="fxHurl">Hurl it</option>
                </select>
            </div>
            <div id="component" class="component component-small">
                <ul class="itemwrap">
                    {{--<li class="current"><img src="/images/banner1.jpg" alt="img01"/></li>--}}
                    <li class="current"><img src="/37banner/images/01.jpg" alt="img02"/></li>
                    <li><img src="/37banner/images/02.jpg" alt="img03"/></li>
                    <li><img src="/37banner/images/03.jpg" alt="img04"/></li>
                    <li><img src="/37banner/images/01.jpg" alt="img05"/></li>
                </ul>
                <nav>
                    <a class="prev" href="#">Previous item</a>
                    <a class="next" href="#">Next item</a>
                </nav>
            </div>
        </section>
    </div>
    {{--结束--}}
    <div style="padding: .5em;background: #ffffff">
        <a style="display: block;font-size: 0;" href=""><img style="width: 100%;max-height: 90px;"
                                                             src="/images/taqing.jpg"></a>
    </div>
    <div class="div2">
        <ul>
            <li>
                <a href="/at/m/site/1">
                    <img src="/images/xinping.png">
                    <p style="display: inline-block">新品专区<br>
                        <span style="font-size: .8em;color:gray;">新品上架啦</span>
                    </p>
                </a>
            </li>
            <li>
                <a href="/at/m/site/2">
                    <img src="/images/tejia.png">
                    <p style="display: inline-block">特价专区<br>
                        <span style="font-size: .8em;color:gray;">每日特卖欢迎来选</span>
                    </p>
                </a>
            </li>
        </ul>
        <ul style="width: 100%">
            <li>
                <a href="/at/m/site/3">
                    <img src="/images/tuijian.png">
                    <p style="display: inline-block">店长推荐<br>
                        <span style="font-size: .8em;color:gray;">店长推荐值得信赖</span>
                    </p>
                </a>
            </li>
            <li>
                <a href="/at/m/site/4">
                    <img src="/images/remai.png">
                    <p style="display: inline-block">热卖专区<br>
                        <span style="font-size: .8em;color:gray;">看看那些热卖的吧</span>
                    </p>
                </a>
            </li>
        </ul>
    </div>
    <div class="div3">
        <div class="div3-2">
            <img class="div3-2-icon" src="/images/icon-putao.png">
            - 红葡萄酒 -
            {{--<img class="div3-2-icon" src="/images/icon-putao.png">--}}
        </div>
        <div class="div3-1">
            <div class="div3-1-item">
                <img src="/images/1.jpg">
                <p>心动五折</p>
                <a href="javascript:;" class="weui_btn bg-orange-b">立即看看</a>
            </div>
        </div>
    </div>
    <div class="div3">
        <div class="div3-2">
            - 红葡萄酒 -
        </div>
        <div class="div3-1">
            <div class="div3-1-item">
                <img src="/images/1.jpg">
                <p>心动五折</p>
                <a href="javascript:;" class="weui_btn bg-orange-b">立即看看</a>
            </div>
        </div>
    </div>
    <div class="div3">
        <div class="div3-2">
            - 红葡萄酒 -
        </div>
        <div class="div3-1">
            <div class="div3-1-item">
                <img src="/images/1.jpg">
                <p>心动五折</p>
                <a href="javascript:;" class="weui_btn bg-orange-b">立即看看</a>
            </div>
        </div>
    </div>
    <script src="/37banner/js/classie.js"></script>
    <script src="/37banner/js/main.js"></script>
@endsection
@section('js')
    <script type="text/javascript">
        $("#a-home").attr("href", "javascript:;");
        $("#img-home").attr("src", "/images/icon-home-active.png");
        $("#menu-name-home").css("color", "#6f599c");
        $(document).ready(function () {
            var winHeight = $(document).scrollTop();
            $(window).scroll(function () {
                var scrollY = $(document).scrollTop();// 获取垂直滚动的距离，即滚动了多少
                if (scrollY > 60) { //如果滚动距离大于550px则隐藏，否则删除隐藏类
                    $('#top-bar').addClass('hidden');
                }
                else {
                    $('#top-bar').removeClass('hidden');
                }

                if (scrollY > winHeight) { //如果没滚动到顶部，删除显示类，否则添加显示类
                    $('#top-bar').removeClass('showed');
                }
                else {
                    $('#top-bar').addClass('showed');
                }

            });
        });
    </script>
@endsection