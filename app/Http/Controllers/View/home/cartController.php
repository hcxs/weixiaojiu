<?php

namespace App\Http\Controllers\View\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;

class cartController extends Controller
{
    public function toCart()
    {
        if (Cart::isEmpty()) {
            return view('home.page.blank_cart');
        }
        return view('home.page.cart')->with('items', Cart::getContent())
            ->with('total', Cart::getTotal());
    }
}
