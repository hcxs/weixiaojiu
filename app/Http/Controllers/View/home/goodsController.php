<?php

namespace App\Http\Controllers\View\home;

use App\Model\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class goodsController extends Controller
{
    public function toGoods($id)
    {
        $goods = Goods::find($id);
//        ->with('goods', $goods)
        return view('home.page.goods');
    }
}
