<?php

namespace App\Http\Controllers\View\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Attributes;
use App\Model\Goods;

class categoryController extends Controller
{
    public function toSite($id)
    {
        $items = Attributes::where('attribute', $id)->get();
        return view('home.page.site')->with('items', $items);
    }

    public function toGoods($id)
    {
        $goods = Goods::find($id);
        return view('home.page.show')->with('goods', $goods);
    }
}
