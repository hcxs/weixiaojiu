<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Tools\Pusher;
use App\Model\Goods;

class cartController extends Controller
{
    public function add()
    {
        $push = new Pusher();
        $goods = Goods::find($_GET['id']);
        $price = $goods->attr->cut * $goods->price * 0.1;
        Cart::add($goods->id, $goods->name, $price, $_GET['num'], array('origin_price' => $goods->price, 'preview' => $goods->preview, 'summary' => $goods->attr->summary));
        $push->status = 0;
        $push->data = $price;
        $push->message = "添加成功";
        return $push->toJson();
    }

    public function inc()
    {
        $push = new Pusher();
        $goods = Goods::find($_GET['id']);
        $price = $goods->attr->cut * $goods->price * 0.1;
        Cart::update($goods->id, array('quantity' => -1));
        $push->status = 0;
        $push->data = $price;
        return $push->toJson();
    }

    public function remove()
    {
        $push = new Pusher();
        $price = Cart::get($_GET['id'])->getPriceSum();
        Cart::remove($_GET['id']);
        $push->status = 0;
        $push->data = $price;
        $push->message = "删除成功";
        return $push->toJson();
    }

}
